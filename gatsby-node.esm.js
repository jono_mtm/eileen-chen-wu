require("ts-node").register({ files: true });

import { resolve } from "path";

export async function createPages({ graphql, actions }) {
    const { createPage } = actions;

    const results = await graphql(`
        {
            allDatoCmsProject {
                edges {
                    node {
                        id
                        slug
                    }
                }
            }
        }
    `);

    if (results.errors) throw results.errors;

    const Project = resolve("./src/templates/project.tsx");

    results.data.allDatoCmsProject.edges.forEach(({ node }) => {
        createPage({
            path: `projects/${node.slug}`,
            component: Project,
            context: {
                id: node.id,
            },
        });
    });
}
