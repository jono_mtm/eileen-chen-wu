<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
  <a href="https://www.gatsbyjs.com">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h3 align="center">
  Open Weaves
</h3>
<p align="center">Perennial designs, world's finest yarns.</p>

<hr/>

<p align="center">Built with Gatsby, Jonathan Vu 2020.</p>

