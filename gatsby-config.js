require("dotenv").config({
    path: `.env`,
});

module.exports = {
    pathPrefix: "/",
    siteMetadata: {
        siteUrl: "https://www.eileen-chen-wu.netlify.app",
        pathPrefix: "/",
        title: `Eileen Chen Wu`,
        titleAlt: `Eileen Chen Wu`,
        description: `Hi. I'm Eileen, a communication designer from Australia pursuiing a second degree in Creative Direction fro Fashion in London. My portfolio is built with a passion for print design as well as Brand Identity, Illustration & Photography.`,
        // banner: "./static/banner.png",
        headline: "",
        siteLanguage: "en",
        ogLanguage: "en_US",
        author: "Eileen Chen Wu",
    },
    plugins: [
        "gatsby-plugin-typescript",
        {
            resolve: `gatsby-source-datocms`,
            options: {
                // You can find your read-only API token under the Settings > API tokens
                // section of your administrative area:
                apiToken: process.env.GATSBY_DATACMS_READONLY_API_TOKEN,

                // // The project environment to read from. Defaults to the primary environment:
                // environment: `master`,

                // If you are working on development/staging environment, you might want to
                // preview the latest version of records instead of the published one:
                previewMode: false,

                // Disable automatic reloading of content when some change occurs on DatoCMS:
                disableLiveReload: false,

                // Custom API base URL (most don't need this)
                // apiUrl: 'https://site-api.datocms.com',

                // Setup locale fallbacks
                // In this example, if some field value is missing in Italian, fall back to English
                localeFallbacks: {
                    it: ["en"],
                },
            },
        },
    ],
};
