import styled from "@emotion/styled";
import { graphql } from "gatsby";
import React, { useState } from "react";
import ReactMarkdown from "react-markdown";
import Global from "../components/Global";
import Header from "../components/Header";
import RelatedProjects from "../components/RelatedProjects";
import Sections from "../components/Sections";
import Col from "../components/ui/Col";
import Img from "../components/ui/Img";
import LightBox from "../components/ui/LightBox";
import Row from "../components/ui/Row";

const Project = ({ data }) => {
    const project = data.datoCmsProject;
    const { title, introduction, content, banner } = project;
    const [src, setSrc] = useState<string>("");
    const [isLightBoxVisible, setLightBoxVisible] = useState<boolean>(false);
    const flattenedImages = content
        .map(
            (section) =>
                section.internal.type === "DatoCmsImagesSection" &&
                section.images.map((image) => image.url)
        )
        .filter((url) => url)
        .flat();

    return (
        <Global color="black">
            {isLightBoxVisible && (
                <LightBox
                    images={flattenedImages}
                    src={src}
                    setSrc={setSrc}
                    close={() => setLightBoxVisible(false)}
                />
            )}
            <Header />
            <Wrapper>
                <Img src={banner?.url} />
                <Row justify="center">
                    <Col span={16}>
                        <h1 className="hanwang med textalign-center">
                            {title}
                        </h1>
                        <div className="paragraph">
                            <ReactMarkdown>{introduction}</ReactMarkdown>
                        </div>
                    </Col>
                </Row>
                <Sections
                    sections={content}
                    showLightBox={() => setLightBoxVisible(true)}
                    setSrc={setSrc}
                />
                <RelatedProjects currentProjectId={project.id} />
            </Wrapper>
        </Global>
    );
};

export default Project;

const Wrapper = styled.div`
    position: relative;
    margin: auto;
    max-width: 1200px;
    @media (max-width: 1000px) {
        padding: 1rem;
        box-sizing: border-box;
    }
`;

export const query = graphql`
    query getProject($id: String!) {
        datoCmsProject(id: { eq: $id }) {
            id
            slug
            title
            introduction
            # meta {
            #     publishedAt(formatString: "dddd MM yyyy")
            # }
            banner {
                url
            }
            content {
                ... on DatoCmsImagesSection {
                    internal {
                        type
                    }
                    id
                    images {
                        url
                        title
                    }
                    justify
                    verticalAlignment
                    widthPercentage
                }
                ... on DatoCmsTextSection {
                    internal {
                        type
                    }
                    id
                    text
                    columns
                    justify
                    widthPercentage
                }
                ... on DatoCmsVideoSection {
                    internal {
                        type
                    }
                    id
                    link {
                        url
                    }
                }
            }
        }
    }
`;
