import styled from "@emotion/styled";
import { Link } from "gatsby";
import React, { useState } from "react";
import Cover from "./ui/Cover";

const ProjectCard: React.FC<{ project: any }> = ({ project }) => {
    const [isHovered, setHovered] = useState<boolean>(false);
    return (
        <Link to={`/projects/${project.slug}`}>
            <Wrapper
                onMouseOver={() => setHovered(true)}
                onMouseLeave={() => setHovered(false)}
            >
                <ImageWrapper>
                    {isHovered ? (
                        <Cover
                            src={
                                project.thumbnailHovered?.url ||
                                project.thumbnail.url
                            }
                        />
                    ) : (
                        <Cover src={project.thumbnail.url} />
                    )}
                </ImageWrapper>

                <p className="paragraph">{project.title}</p>
            </Wrapper>
        </Link>
    );
};

export default ProjectCard;

const ImageWrapper = styled.div`
    overflow: hidden;
    height: 400px;
    margin-bottom: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    margin-bottom: 3rem;
    @media (max-width: 1000px) {
        margin-bottom: 0.5rem;
    }
`;
