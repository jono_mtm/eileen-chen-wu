import React from "react";
import Video from "../ui/Video";

const VideoSection: React.FC<{ section }> = ({ section }) => {
    return <Video link={section.link.url} />;
};

export default VideoSection;
