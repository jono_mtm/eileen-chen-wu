import React from "react";
import Md from "../Md";
import Row from "../ui/Row";

const TextSection: React.FC<{ section }> = ({ section }) => {
    const { justify, widthPercentage } = section;

    const getJustify = () => {
        if (justify === "left") return "flex-start";
        if (justify === "center") return "center";
        if (justify === "right") return "flex-end";
    };

    return (
        <Row justify={getJustify()}>
            <div style={{ width: `${widthPercentage}%` }}>
                <Md>{section.text}</Md>
            </div>
        </Row>
    );
};

export default TextSection;
