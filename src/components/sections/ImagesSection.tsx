import React from "react";
import { Desktop, Mobile } from "../responsive";
import Col from "../ui/Col";
import Img from "../ui/Img";
import Row from "../ui/Row";

const ImagesSection: React.FC<{
    section: any;
    showLightBox: () => void;
    setSrc: (string) => void;
}> = ({ section, showLightBox, setSrc }) => {
    const { justify, verticalAlignment, widthPercentage } = section;

    const getJustify = () => {
        if (justify === "left") return "flex-start";
        if (justify === "center") return "center";
        if (justify === "right") return "flex-end";
    };

    const getAlign = () => {
        if (verticalAlignment === "top") return "flex-start";
        if (verticalAlignment === "middle") return "center";
        if (verticalAlignment === "bottom") return "flex-end";
    };

    return (
        <Row justify={getJustify()} align={getAlign()} gutter="5px">
            {section.images.map((image) => (
                <ImageWrapper
                    showLightBox={showLightBox}
                    setSrc={setSrc}
                    image={image}
                    width={`${widthPercentage / section.images.length}%`}
                />
            ))}
        </Row>
    );
};

export default ImagesSection;

const ImageWrapper: React.FC<{
    width: string;
    image: any;
    showLightBox: () => void;
    setSrc: (src: string) => void;
}> = ({ width, image, showLightBox, setSrc }) => {
    const desktop = () => (
        <Desktop>
            <div
                style={{
                    display: "inline-block",
                    width,
                }}
                onClick={() => {
                    showLightBox();
                    setSrc(image.url);
                }}
                className="images-section_image"
            >
                <Img src={image.url} />
                {image.title && <p className="caption">{image.title}</p>}
            </div>
        </Desktop>
    );

    const mobile = () => (
        <Mobile>
            <div
                style={{
                    width: "100%",
                }}
                onClick={() => {
                    showLightBox();
                    setSrc(image.url);
                }}
                className="images-section_image"
            >
                <Img src={image.url} />
                {image.title && <p className="caption">{image.title}</p>}
            </div>
        </Mobile>
    );

    return (
        <>
            {mobile()}
            {desktop()}
        </>
    );
};
