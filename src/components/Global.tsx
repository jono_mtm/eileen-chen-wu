import styled from "@emotion/styled";
import React from "react";

import "./global.css";

const Global: React.FC<{ color?: string }> = ({ children, color }) => {
    return <Wrapper color={color}>{children}</Wrapper>;
};

export default Global;

const Wrapper = styled.div<{ color?: string }>`
    min-height: 100vh;
    background: ${(props) => props.color};
    & * {
        color: ${(props) => (props.color === "black" ? "white" : undefined)};
    }
`;
