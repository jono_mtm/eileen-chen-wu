import styled from "@emotion/styled";
import React from "react";
import ImagesSection from "./sections/ImagesSection";
import TextSection from "./sections/TextSection";
import VideoSection from "./sections/VideoSection";

const Sections: React.FC<{
    sections: any;
    showLightBox: () => void;
    setSrc: (src: string) => void;
}> = ({ sections, showLightBox, setSrc }) => {
    return (
        <>
            {sections.map((section) => {
                if (section.internal.type === "DatoCmsImagesSection")
                    return (
                        <StyledSection>
                            <ImagesSection
                                section={section}
                                showLightBox={showLightBox}
                                setSrc={setSrc}
                            />
                        </StyledSection>
                    );

                if (section.internal.type === "DatoCmsTextSection")
                    return (
                        <StyledSection>
                            <TextSection section={section} />
                        </StyledSection>
                    );

                if (section.internal.type === "DatoCmsVideoSection")
                    return (
                        <StyledSection>
                            <VideoSection section={section} />
                        </StyledSection>
                    );
            })}
        </>
    );
};

export default Sections;

const StyledSection = styled.div`
    padding: 3rem 0;
    @media (max-width: 1000px) {
        padding: 0.5rem 0;
    }
`;
