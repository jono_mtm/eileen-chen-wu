import React from "react";
import ReactMarkdown from "react-markdown";

const renderers = {
    image: ({ src }) => {
        return <img className="inline-image" src={src} />;
    },
    emphasis: ({ node }) => {
        return <span className="hanwang">{node.children[0].value}</span>;
    },
    strong: ({ value }) => {
        return <span className="strong">{value}</span>;
    },
    inlineCode: ({ value }) => {
        return <span className="doppelganger">{value}</span>;
    },
};

const Md = ({ children }) => {
    return <ReactMarkdown renderers={renderers}>{children}</ReactMarkdown>;
};

export default Md;
