import styled from "@emotion/styled";
import React from "react";
import { Desktop, Mobile } from "../responsive";

const Row: React.FC<{
    span?: number;
    gutter?: string;

    justify?:
        | "flex-start"
        | "flex-end"
        | "center"
        | "space-around"
        | "space-between";
    align?:
        | "flex-start"
        | "flex-end"
        | "center"
        | "space-around"
        | "space-between";
}> = ({ children, span, justify, gutter, align }) => {
    const mobile = () => <Mobile>{children}</Mobile>;

    const desktop = () => (
        <Desktop>
            <StyledRow
                span={span}
                justify={justify}
                gutter={gutter}
                align={align}
            >
                {children}
            </StyledRow>
        </Desktop>
    );

    return (
        <>
            {mobile()}
            {desktop()}
        </>
    );
};

export default Row;

const StyledRow = styled.div<{
    justify?:
        | "flex-start"
        | "flex-end"
        | "center"
        | "space-around"
        | "space-between";
    align?:
        | "flex-start"
        | "flex-end"
        | "center"
        | "space-around"
        | "space-between";
    span?: number;
    gutter?: string;
}>`
    width: ${(props) => (props.span ? `${props.span / 0.24}%` : "100%")};
    display: flex;
    justify-content: ${(props) => props.justify || "left"};
    flex-wrap: wrap;

    align-items: ${(props) => props.align || "flex-start"};

    & > * {
        box-sizing: border-box;
        height: min-content;
        padding: ${(props) => `${props.gutter}` || undefined};
    }

    @media (max-width: 1000px) {
        & > * {
            padding: ${(props) => `${props.gutter} 0` || undefined};
        }
    }
`;
