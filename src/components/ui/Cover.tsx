import styled from "@emotion/styled";
import React, { useState } from "react";

const Cover: React.FC<{
    placeholder?: string;
    src?: string;
    height?: string;
    width?: string;
}> = ({ placeholder, src = "", height = "100%", width = "100%" }) => {
    const [isImageLoading, setImageLoading] = useState<boolean>(true);

    return (
        <>
            <img
                src={src}
                style={{ display: "none" }}
                alt={src}
                onLoad={() => setImageLoading(false)}
            />

            {/* <StyledCover
                className="image placeholderImage"
                src={placeholder || src}
                height={height}
                width={width}
            /> */}

            <StyledCover
                className={`image ${!isImageLoading ? "fade-in" : ""}`}
                src={src}
                height={height}
                width={width}
            />
        </>
    );
};

export default Cover;

const StyledCover = styled.div<{
    src: string;
    height: string;
    width: string;
}>`
    background-image: ${(props) => `url('${props.src}')`};
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    width: ${(props) => props.width};
    height: ${(props) => props.height};
    max-width: 100vw;
`;
