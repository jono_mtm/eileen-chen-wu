import styled from "@emotion/styled";
import React, { useState } from "react";

const Img: React.FC<{
    placeholder?: string;
    src?: string;
    width?: string;
}> = ({ placeholder, src = "", width = "100%" }) => {
    const [isImageLoading, setImageLoading] = useState<boolean>(true);

    return (
        <>
            <img
                src={src}
                style={{ display: "none" }}
                alt={src}
                onLoad={() => setImageLoading(false)}
            />

            {/* <StyledImg
            className="image placeholderImage"
            src={placeholder || src}
            width={width}
            /> */}

            <StyledImg
                className={`image ${!isImageLoading ? "fade-in" : ""}`}
                src={src}
                width={width}
            />
        </>
    );
};

export default Img;

const StyledImg = styled.img<{ width: string }>`
    display: inline-block;
    height: auto;
    width: ${(props) => props.width};
    opacity: 0;

    @media (max-width: 1000px) {
        height: auto;
        width: 100%;
    }
`;
