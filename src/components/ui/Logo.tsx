import styled from "@emotion/styled";
import { Link } from "gatsby";
import React from "react";
import LogoSrc from "../../images/logo.gif";

const Logo = () => {
    return (
        <Link to="/">
            <Wrapper>
                <img src={LogoSrc} alt="logo" style={{ height: "100%" }} />
            </Wrapper>
        </Link>
    );
};

export default Logo;

const Wrapper = styled.div`
    height: 70px;
`;
