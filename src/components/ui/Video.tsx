import React from "react";
import ReactPlayer from "react-player";

const Video: React.FC<{ link: string }> = ({ link }) => {
    return <ReactPlayer width={"100%"} url={link} controls />;
};

export default Video;
