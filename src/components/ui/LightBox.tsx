import styled from "@emotion/styled";
import React, { useState } from "react";
import Col from "./Col";
import Img from "./Img";
import Row from "./Row";

const LightBox: React.FC<{
    images: string[];
    src: string;
    setSrc: React.Dispatch<React.SetStateAction<string>>;
    close: () => void;
}> = ({ images, src, setSrc, close }) => {
    const currentImageIndex = images.indexOf(src);
    return (
        <Wrapper>
            <CloseButton className="controls" onClick={close}>
                x
            </CloseButton>
            <Row justify="center" align="center">
                <div
                    style={{
                        display: "flex",
                        justifyContent: "flex-start",
                        alignItems: "center",
                        height: "100vh",
                        width: "30vw",
                        position: "absolute",
                        left: 0,
                        margin: "3rem",
                    }}
                    onClick={() =>
                        setSrc(
                            currentImageIndex === 0
                                ? images[images.length - 1]
                                : images[currentImageIndex - 1]
                        )
                    }
                >
                    <button className="controls">{"<"}</button>
                </div>
                <div
                    style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        height: "100vh",
                    }}
                >
                    <LightBoxImage src={src} />
                </div>
                <div
                    style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        alignItems: "center",
                        height: "100vh",
                        width: "30vw",
                        position: "absolute",
                        right: 0,
                        margin: "3rem",
                    }}
                    onClick={() =>
                        setSrc(
                            currentImageIndex === images.length - 1
                                ? images[0]
                                : images[currentImageIndex + 1]
                        )
                    }
                >
                    <button className="controls">{">"}</button>
                </div>
            </Row>
        </Wrapper>
    );
};

export default LightBox;

const Wrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    background: rgba(255, 255, 255, 0.9);
    z-index: 1;
`;

const LightBoxImage = styled.img`
    max-height: 100vh;
    max-width: 100vw;
    height: auto;
    width: auto;
`;

const CloseButton = styled.button`
    position: absolute;
    right: 0;
    top: 0;
    margin: 3rem;
`;
