import styled from "@emotion/styled";
import React from "react";
import { Desktop, Mobile } from "../responsive";

const Col: React.FC<{
    span?: number;
    onClick?: () => void;
}> = ({ children, span, onClick }) => {
    const mobile = () => (
        <Mobile>
            <StyledCol className="column" span={24} onClick={onClick}>
                {children}
            </StyledCol>
        </Mobile>
    );

    const desktop = () => (
        <Desktop>
            <StyledCol className="column" span={span} onClick={onClick}>
                {children}
            </StyledCol>
        </Desktop>
    );

    return (
        <>
            {mobile()}
            {desktop()}
        </>
    );
};

export default Col;

const StyledCol = styled.div<{
    span?: number;
}>`
    display: block;
    flex: ${(props) => (props.span ? `0 0 ${props.span / 0.24}%` : "auto")};
    max-width: ${(props) =>
        props.span ? `${props.span / 0.24}%` : "fit-content"};
    @media (max-width: 1000px) {
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
`;
