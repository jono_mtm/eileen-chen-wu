import styled from "@emotion/styled";
import React from "react";
import { useProjects } from "../utils/queries/projects";
import ProjectCard from "./ProjectCard";
import { Desktop, Mobile } from "./responsive";
import Col from "./ui/Col";
import Row from "./ui/Row";

const RelatedProjects: React.FC<{ currentProjectId: string }> = ({
    currentProjectId,
}) => {
    const { projects } = useProjects();
    const filteredProjects = projects
        .filter((project) => project.id !== currentProjectId)
        .filter((p, i) => i < 4);

    const mobile = () => (
        <Mobile>
            <Wrapper>
                <h3 className="small">Up Next:</h3>
                <ImagesWrapper>
                    <Row gutter="5px">
                        <ProjectCard project={projects[0]} />
                    </Row>
                </ImagesWrapper>
            </Wrapper>
        </Mobile>
    );

    const desktop = () => (
        <Desktop>
            <Wrapper>
                <h3 className="small">You may also like:</h3>
                <ImagesWrapper>
                    <Row gutter="5px">
                        {filteredProjects.map((project) => (
                            <Col span={6}>
                                <ProjectCard project={project} />
                            </Col>
                        ))}
                    </Row>
                </ImagesWrapper>
            </Wrapper>
        </Desktop>
    );
    return (
        <>
            {mobile()}
            {desktop()}
        </>
    );
};

export default RelatedProjects;

const Wrapper = styled.div`
    padding: 3rem 0;
    @media (max-width: 1000px) {
        padding: 0.5rem 0;
    }
`;

const ImagesWrapper = styled.div`
    text-align: center;
`;
