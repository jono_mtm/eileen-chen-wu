import styled from "@emotion/styled";
import { Link } from "gatsby";
import React from "react";
import { Desktop, Mobile } from "./responsive";
import Logo from "./ui/Logo";
import Row from "./ui/Row";

const Header: React.FC<{ slug?: string }> = ({ slug }) => {
    const mobile = () => {
        return (
            <Mobile>
                <MobileWrapper>
                    <Row justify="space-between">
                        <nav>
                            <Link
                                className={`navlink ${
                                    slug === "portfolio" ? "active" : ""
                                }`}
                                to="/portfolio"
                            >
                                Portfolio
                            </Link>
                            <Link className="navlink" to="/about">
                                About
                            </Link>
                            <Link className="navlink" to="/contact">
                                Contact
                            </Link>
                        </nav>
                    </Row>
                    <br />
                    <Logo />
                </MobileWrapper>
            </Mobile>
        );
    };

    const desktop = () => {
        return (
            <Desktop>
                <Wrapper>
                    <Row justify="space-between">
                        <nav>
                            <Link
                                className={`navlink ${
                                    slug === "portfolio" ? "active" : ""
                                }`}
                                to="/portfolio"
                            >
                                Portfolio
                            </Link>
                            <Link className="navlink" to="/about">
                                About
                            </Link>
                            <Link className="navlink" to="/contact">
                                Contact
                            </Link>
                        </nav>
                        <Logo />
                    </Row>
                </Wrapper>
            </Desktop>
        );
    };

    return (
        <>
            {mobile()}
            {desktop()}
        </>
    );
};

export default Header;

const Wrapper = styled.header`
    padding: 2.5rem;
`;

const MobileWrapper = styled.header`
    padding: 1rem;
`;
