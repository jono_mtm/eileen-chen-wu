import { graphql, useStaticQuery } from "gatsby";
import { find } from "lodash";

export const useProjects = () => {
    const data = useStaticQuery(
        graphql`
            query getAllProjects {
                allDatoCmsProject {
                    edges {
                        node {
                            id
                            slug
                            title
                            thumbnail {
                                url
                            }
                            thumbnailHovered {
                                url
                            }
                            meta {
                                publishedAt(formatString: "dddd MM yyyy")
                            }
                        }
                    }
                }
            }
        `
    );

    const projects = data.allDatoCmsProject.edges.map(({ node }) => {
        return node;
    });

    return { projects };
};

export const useFindProject = (id: string) => {
    const { projects } = useProjects();

    const project = find(projects, { id });

    return { project };
};
