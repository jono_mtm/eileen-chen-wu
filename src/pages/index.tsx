import { navigate } from "gatsby";

export default () => {
    if (typeof window === `undefined`) return null;
    navigate("/about");

    return null;
};
