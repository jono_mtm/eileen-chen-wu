import styled from "@emotion/styled";
import React from "react";
import Global from "../components/Global";

const Contact = () => {
    return (
        <Global color="rgb(238, 70, 52)">
            <Wrapper>
                <a
                    className="hanwang med"
                    target="_blank"
                    href="mailto:eln.ch.wu@gmail.com"
                >
                    {"→ eln.ch.wu@gmail.com"}
                </a>
            </Wrapper>
        </Global>
    );
};

export default Contact;

const Wrapper = styled.div`
    height: 100vh;
    margin-left: 3rem;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    @media (max-width: 1000px) {
        margin-left: 1rem;
    }
`;
