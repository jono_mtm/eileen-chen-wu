import styled from "@emotion/styled";
import React from "react";
import Global from "../components/Global";
import Header from "../components/Header";
import ProjectCard from "../components/ProjectCard";
import Col from "../components/ui/Col";
import Row from "../components/ui/Row";
import { useProjects } from "../utils/queries/projects";

const Portfolio = () => {
    const { projects } = useProjects();
    return (
        <Global color="black">
            <Header slug="portfolio" />
            <Wrapper>
                <Row gutter="5px">
                    {projects.map((project) => (
                        <Col span={12}>
                            <ProjectCard project={project} />
                        </Col>
                    ))}
                </Row>
            </Wrapper>
        </Global>
    );
};

export default Portfolio;

const Wrapper = styled.div`
    position: relative;
    margin: auto;
    max-width: 1200px;
    @media (max-width: 1000px) {
        padding: 1rem;
        box-sizing: border-box;
    }
`;
