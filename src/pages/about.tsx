import styled from "@emotion/styled";
import { Link } from "gatsby";
import { graphql } from "gatsby";
import React from "react";
import Global from "../components/Global";
import Md from "../components/Md";

const About = ({ data }) => {
    const aboutPage = data.allDatoCmsAboutPage.edges[0].node;
    const { content } = aboutPage;
    return (
        <Global color="rgb(238, 70, 52)">
            <Wrapper>
                <div className="heading">
                    <Md>{content}</Md>
                </div>
                <Link to="/portfolio" style={{ textAlign: "center" }}>
                    <p className="hanwang">{"Portfolio →"}</p>
                </Link>
            </Wrapper>
        </Global>
    );
};

export default About;

const Wrapper = styled.div`
    padding: 1rem;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    min-height: 100vh;
`;

export const query = graphql`
    query getAboutPage {
        allDatoCmsAboutPage {
            edges {
                node {
                    content
                }
            }
        }
    }
`;
